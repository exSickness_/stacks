#ifndef STACK_H
	#define STACK_H

#include <stdbool.h>

#ifdef STACK_ARRAY

#include <stdlib.h>
/* Array implementation */
typedef stack{
	size_t head;
	size_t capacity;
	double* data;
};

#else

/* Linked List Implementation */
#include <llish.h>
typedef stack{
	struct llist* head;
} stack;

#endif

stack* stack_create(void);
void stack_destroy(stack* s);

bool stack_is_empty(stack* s);
void stack_push(stack* s, double data);
double stack_pop(stack* s);

#endif