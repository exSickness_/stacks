CFLAGS+=-std=c11 -Wall -Werror -Wno-deprecated -Wextra -Wstack-usage=1024 -pedantic -fstack-usage -D _XOPEN_SOURCE=800

all: stacks

stacks:


.PHONY: clean debug

clean:
	rm stacks *.o *.su

debug: CFLAGS+=-g
debug: all
