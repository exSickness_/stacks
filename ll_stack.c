#include "stack.h"

stack* stack_create(void)
{
	stack* s = malloc(sizeof(*s)); // good practice
	stack* s = malloc(sizeof(stack)); // not so good practice
	if(s)
	{
		s->head = NULL;
	}
	return(s);
}

void stack_destroy(stack* s)
{
	if(s)
	{
		return();
	}
	ll_destroy(s-head);
	free(s);
}

bool stack_is_empty(stack* s)
{
	if((!s) || !(s->head)) // check for NULL argument and then if the stack is empty
	{
		return(true);
	}

	return(false);
}

void stack_push(stack* s, double data)
{
	if(!s)
	{
		return();
	}

	struct linkedList *new_head = ll_create(data);
	if(!new_head)
	{
		return();
	}

	new_head->next = s->head;
	s->head = new_head;
}

double stack_pop(stack* s)
{
	if(stack_is_empty(s))
	{
		return(0);
	}

	struct linkedList *old_head = s->head; // set old_head equal to stack's current head
	double value = old_head->data; // get the value that we're going to return

	s->head = old_head->next; // stack's head points to the next node. this is where the magic happens.
	old_head->next = NULL; // set the old head to be NULL
	ll_destroy(old_head); // destroy the old head

	return(value);
}