#ifndef LLIST_H
	#define LLIST_H

#include <stdbool.h>

struct linkedList{
	void* data;
	struct linkedList* next;
};


void* llist_remove(struct linkedList** list);
void llist_add(struct linkedList** list, void* value); // add node as head
void llist_disassemble( struct linkedList* list);
bool ll_is_sorted(struct linkedList* list, int (*cmp)(void* x,void* y));
void llist_destroy( struct linkedList* list);
struct linkedList* llist_create(void* value);
// bool ll_equal_by(struct linkedList* a, struct linkedList* b, int (*cmp)(void* x,void* y));
int cmp(void* x, void* y);
bool ll_is_circular(struct linkedList *list);
void ll_reverse(struct linkedList **list);
// void ll_insert_sorted(struct linkedList **list, void* value);

#endif